/*---------------------------------
 * React Component Store Server
 * @Author:        Rupert Ben Wiser
 * @Version:       1.0.1
 * @Date:          16 June 2017
 *--------------------------------*/

// This is the server required for the react component store 
// extension for visual studio code to work.

'use strict';
let express = require("express");
let bodyparser = require("body-parser");
let fs = require("fs");
let crypto = require("crypto");
let sqlite = require("sqlite3");
let configSettings = require("./config");

let app = express();

const PORT = configSettings.port;

const defaultConfig = {
	storedCode: [],
	componentCode: {}
};

let config = defaultConfig;

// If the database does not exist it will be generated here
// the latest configuration is then retrieved

// The reason this is stored the way is it allows for version control

(() => {
	let db = new sqlite.Database(configSettings.database);
	db.serialize(() => {
		db.run("CREATE TABLE if not exists config (id INTEGER PRIMARY KEY, data TEXT)");

		db.each("SELECT * FROM config", (err, row) => {
			config = JSON.parse(row.data);
		});
	});
	db.close();
})();


const saveChanges = () => {
	const stringedConfig = JSON.stringify(config);

	let db = new sqlite.Database(configSettings.database);
	db.serialize(() => {
		db.run(`INSERT INTO config (data) VALUES ('${stringedConfig}')`);
	});
	db.close();
};

// This is required so that the client side extension can post changes
// of code to the server.

app.use(bodyparser.json());


app.get("/suggestions", (req, res) => {
    res.send(config);
});

app.get("/component/:name", (req, res) => {
	let code = config.componentCode[req.params.name];
	res.send(code);
});

app.get("/version", (req, res) => {
	let stringedConfig = JSON.stringify(config);
	let hash = crypto.createHmac("sha1", "SECRET")
					.update(stringedConfig)
					.digest("hex");

	res.send(hash);
});

app.post("/push", (req, res) => {
	let dataLines = req.body.docLines;
	let componentName = req.body.componentName;

	let linesTogether = dataLines.reduce((accu, next) => accu+"\n"+next);

	let propertyList = [];
	let propertyRegExp = /props\.\w+\}/g;

	let line = null;

	while ( (line = propertyRegExp.exec(linesTogether)) != null ) {
		const property = line[0].replace(/props\./, "").replace(/\}/, "");
		if ( propertyList.indexOf(property) < 0 ) propertyList.push(property);
	}

	let foundItem = config.componentCode[componentName];

	if (!foundItem) {
		config.storedCode.push({
			propertyList,
			name: componentName
		});
	} else {
		config.storedCode.filter(suggestion => suggestion.name === componentName)
							.forEach(suggestion => suggestion.propertyList = propertyList);
	}

	config.componentCode[componentName] = linesTogether;

	saveChanges();
});

console.log(`
-------------------------------------
REACT COMPONENT STORE SERVER STARTING
-------------------------------------`);

console.log(`Listening on port ${PORT}`);
app.listen(PORT);