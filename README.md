# REACT COMPONENT STORE SERVER README

This is the react component store server for the visual
studio plugin.

The purpose of this server is to provide a server that
will maintain all reusable components for a team to push to
that may be reused among different project.

## Commands

To start the server cd into it's directory and type:
npm start

## Requirements

Once this server is hosted you will need the react component
store client extension in order to use it.

### 1.0.0

Initial API calls for version, suggestions, component, and push
have been added.